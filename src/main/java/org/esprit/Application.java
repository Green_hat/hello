package org.esprit;

import org.esprit.beans.Rectangle;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class Application {
    public static void main(String[] args) {
        BeanFactory context = new XmlBeanFactory(new ClassPathResource("context.xml"));
        Rectangle r= (Rectangle) context.getBean("monRectangle");
        System.out.println(r.getLongueur());
r.setLongueur(0);
        Rectangle r2= (Rectangle) context.getBean("monRectangle");
        System.out.println(r2.getLongueur());
    }
}
