package org.esprit.beans;

public class Cercle {
    private Integer diametre;

    public Integer getDiametre() {
        return diametre;
    }

    public void setDiametre(Integer diametre) {
        this.diametre = diametre;
    }
}
